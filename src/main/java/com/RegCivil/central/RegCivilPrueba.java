package com.RegCivil.central;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.*;

@RestController
@EnableAutoConfiguration
public class RegCivilPrueba {
	
	@CrossOrigin(origins = { "http://localhost:4200" })
	@RequestMapping("regCivilPrueba")
	String prueba() {
		return "{'prueba':'Prueba Registro Civil 2020'}";
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(RegCivilPrueba.class, args);
	}

}
